FROM kong:2.4.0
#FROM kong:2.3.1
LABEL description="Kong 2.4.0 + kong-oidc plugin + zipkin-tracing"

USER root
#RUN apk update && apk add git unzip luarocks
#RUN luarocks install kong-oidc
RUN luarocks install zipkin-tracing
#RUN luarocks install kong-oidc

USER kong
